import { MAX_PROGRESS_BAR_WIDTH } from "../public/javascript/config.mjs";

// Facade pattern
export class RoomsFacade {
  getConnectedUserRoomName(rooms, username) {
    return Object.keys(rooms).find((roomName) =>
      rooms[roomName].some((user) => user.username === username)
    );
  }

  removeUserFromRoom(rooms, username) {
    for (const roomName in rooms) {
      if (rooms.hasOwnProperty(roomName)) {
        rooms[roomName] = rooms[roomName].filter(
          (user) => user.username !== username
        );
        if (rooms[roomName].length === 0) {
          this.deleteRoom(roomName);
        }
      }
    }
  }

  deleteRoom(rooms, roomName) {
    delete rooms[roomName];
  }

  clearUserProgress(rooms, roomName) {
    rooms[roomName] = rooms[roomName].map((user) => ({
      ...user,
      ready: false,
      progressBarWidth: 0,
      timeFinished: 0,
    }));
  }
}

export const getLeaderBoard = (users) => {
  let sortedUsers = users;
  sortedUsers.sort(function (a, b) {
    if (a.progressBarWidth === b.progressBarWidth) {
      return a.timeFinished > b.timeFinished ? 1 : -1;
    }
    return a.progressBarWidth < b.progressBarWidth ? 1 : -1;
  });

  const leaderBoard = sortedUsers.map((user) => user.username);
  return leaderBoard;
};

export const checkAllPlayersDone = (users) => {
  return users.every(
    (user) => user.progressBarWidth === MAX_PROGRESS_BAR_WIDTH
  );
};

export const checkAllPlayerReady = (users) => {
  return users.every((user) => user.ready === true);
};
