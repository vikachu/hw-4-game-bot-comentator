import { BehaviorSubject } from "rxjs";
import { getLeaderBoard } from "../socket/gameHelpers";
import { getTextFactory, getTextFactoryCurried } from "./botTextData";

const gameState = new BehaviorSubject({
  type: undefined,
  state: {
    roomName: undefined,
    users: [],
  },
});

export const actionsTypes = {
  SAY_HELLO: 1,
  INTRODUCE_PLAYERS: 2,
  GAME_STATUS: 3,
  CLOSE_TO_FINISH: 4,
  PLAYER_FINISHED: 5,
  GAME_RESULT: 6,
};

export const getGameState = () => {
  return gameState.asObservable();
};

export const setGameState = ({ action, payload }) => {
  gameState.next({
    type: action,
    state: {
      ...gameState.value.state,
      ...payload
    },
  });
};

export const botComentatorMessage = ({ io, action }) => {
  let botText = "";

  switch (action.type) {
    case actionsTypes.SAY_HELLO:
      botText = getTextFactory("sayHello").message;
      break;

    case actionsTypes.INTRODUCE_PLAYERS:
      const usernames = action.state.users
        .map((user) => user.username)
        .join(", ");
      // currying
      botText = getTextFactoryCurried("introducePlayers")(usernames).message;
      break;

    case actionsTypes.GAME_STATUS:
      const currentLeaders = getLeaderBoard(action.state.users).join(", ");
      botText = getTextFactory("gameStatus", currentLeaders).message;
      break;

    case actionsTypes.CLOSE_TO_FINISH:
      const closeToFinishUser = action.state.username;
      botText = getTextFactory("closeToFinish", closeToFinishUser).message;
      break;

    case actionsTypes.PLAYER_FINISHED:
      const username = action.state.username;
      botText = getTextFactory("playerFinished", username).message;
      break;

    case actionsTypes.GAME_RESULT:
      const leaders = getLeaderBoard(action.state.users).join(", ");
      botText = getTextFactory("gameResults", leaders).message;
      break;

    default:
      break;
  }

  io.to(action.state.roomName).emit("UPDATE_BOT_TEXT", {
    text: botText,
  });
};
