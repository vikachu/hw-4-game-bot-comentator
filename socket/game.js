import * as config from "./config";
import { MAX_PROGRESS_BAR_WIDTH } from "../public/javascript/config.mjs";
import { texts } from "../public/javascript/data.mjs";
import {
  getLeaderBoard,
  checkAllPlayerReady,
  checkAllPlayersDone,
  RoomsFacade,
} from "./gameHelpers";
import {
  setGameState,
  getGameState,
  actionsTypes,
  botComentatorMessage,
} from "../bot/botComentator";

let roomFacade = new RoomsFacade();
let rooms = {};
let usernames = [];

// Proxy pattern
const handler = {
  set(target, property, value) {
    if (value === "user 1") {
      console.log("How long have you thought about username?");
    }
    target[property] = value;
    return true;
  },
};

const proxyUsernames = new Proxy(usernames, handler);

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;
    initNewUserConnection(socket, username);

    socket.on("disconnect", function () {
      disconnectUser({ io, socket, username });
    });

    socket.on("CREATE_ROOM", (roomName) => {
      if (rooms[roomName] !== undefined) {
        socket.emit("ROOM_NAME_EXISTS", roomName);
      } else {
        createRoom({ io, socket, roomName, username });
      }
    });

    socket.on("JOIN_ROOM", (roomName) => {
      if (rooms[roomName].length === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.emit("ROOM_IS_FULL", config.MAXIMUM_USERS_FOR_ONE_ROOM);
      } else {
        joinRoom({ socket, roomName, username });
      }
    });

    socket.on("BACK_TO_ROOMS", (roomName) => {
      roomFacade.removeUserFromRoom(rooms, username);

      if (rooms.hasOwnProperty(roomName)) {
        const users = rooms[roomName];

        io.to(roomName).emit("PLAYER_JEFT", { roomName, users });
      }
      socket.emit("INIT_ROOMS", rooms);
      socket.broadcast.emit("INIT_ROOMS", rooms);
      socket.leave(roomName);
    });

    socket.on("READY_PRESSED", (roomName) => {
      let users = rooms[roomName];
      const userReady = !users.find((user) => user.username === username).ready;
      Object.assign(
        rooms[roomName].find((user) => user.username === username),
        { ready: userReady }
      );

      users = rooms[roomName];
      io.to(roomName).emit("READY_PRESSED_DONE", { roomName, users });

      if (checkAllPlayerReady(users)) {
        setGameState({
          action: actionsTypes.SAY_HELLO,
          payload: {
            roomName: roomName,
            users: rooms[roomName],
            username: username,
          },
        });
        startCountdown({ io, roomName, username });
      }
    });

    socket.on(
      "RIGHT_CHAR_TYPED",
      ({ roomName, progressBarWidth, charsToEnd }) => {
        const user = rooms[roomName].find((user) => user.username === username);
        user.progressBarWidth = progressBarWidth;
        user.timeFinished = Date.now();

        checkPlayerFinishedSetState({
          progressBarWidth,
          rooms,
          roomName,
          username,
        });
        checkCloseToFinishSetState({ charsToEnd, rooms, roomName, username });

        const users = rooms[roomName];
        io.to(roomName).emit("UPDATE_PROGRESS_BAR", { roomName, users });
      }
    );
  });
};

const checkUsernameExists = (username) => {
  return proxyUsernames.includes(username);
};

const initNewUserConnection = (socket, username) => {
  if (checkUsernameExists(username)) {
    socket.emit("USERNAME_EXISTS");
  } else {
    proxyUsernames.push(username);
    socket.emit("INIT_ROOMS", rooms);
  }
};

const disconnectUser = ({ io, socket, username }) => {
  const index = proxyUsernames.indexOf(username);
  proxyUsernames.splice(index, 1);

  const roomName = roomFacade.getConnectedUserRoomName(rooms, username);
  roomFacade.removeUserFromRoom(rooms, username);

  if (roomName !== undefined && checkAllPlayerReady(rooms[roomName])) {
    startCountdown({ io, roomName, username });
  }
  socket.broadcast.emit("INIT_ROOMS", rooms); // to updated joined counter
};

const createRoom = ({ io, socket, roomName, username }) => {
  getGameState().subscribe((action) => {
    botComentatorMessage({ io, action });
  });

  rooms[roomName] = [
    {
      username: username,
      ready: false,
      progressBarWidth: 0,
      timeFinished: 0,
    },
  ];
  const users = rooms[roomName];
  const joinedPlayers = users.length;

  socket.emit("APPEND_ROOM_CARD", { roomName, joinedPlayers });
  socket.broadcast.emit("APPEND_ROOM_CARD", { roomName, joinedPlayers });
  socket.join(roomName, () => {
    socket.emit("JOIN_ROOM_DONE", { roomName, users });
  });
};

const joinRoom = ({ socket, roomName, username }) => {
  const user = {
    username: username,
    ready: false,
    progressBarWidth: 0,
    timeFinished: 0,
  };
  rooms[roomName].push(user);
  const users = rooms[roomName];

  socket.join(roomName, () => {
    socket.emit("JOIN_ROOM_DONE", { roomName, users });
    socket.broadcast.emit("UPDATE_ROOM_PLAYERS", { roomName, users });
    socket.broadcast.emit("INIT_ROOMS", rooms);
  });
};

const startCountdown = ({ io, roomName, username }) => {
  let countdownTime = config.SECONDS_TIMER_BEFORE_START_GAME;

  const countdown = setInterval(function () {
    io.to(roomName).emit("COUNTDOWN_BEFORE_START", {
      roomName,
      countdownTime,
    });

    countdownTime--;
    if (countdownTime < 0) {
      clearInterval(countdown);
      setGameState({
        action: actionsTypes.INTRODUCE_PLAYERS,
        payload: {
          roomName: roomName,
          users: rooms[roomName],
          username: username,
        },
      });
      startGame({ io, roomName, username });
    }
  }, 1000);
};

const startGame = ({ io, roomName, username }) => {
  let timeForGame = config.SECONDS_FOR_GAME;
  const textIndex = Math.floor(Math.random() * texts.length);
  let users = rooms[roomName];

  io.to(roomName).emit("START_GAME", { roomName, textIndex, timeForGame });
  timeForGame--; // to reduce delay

  const gameTimer = setInterval(function () {
    io.to(roomName).emit("GAME_TIMER", { roomName, timeForGame });

    timeForGame--;
    if (showBotGameStatus(timeForGame)) {
      setGameState({
        action: actionsTypes.GAME_STATUS,
        payload: {
          roomName: roomName,
          users: rooms[roomName],
          username: username,
        },
      });
    }
    if (timeForGame < 0 || checkAllPlayersDone(users)) {
      clearInterval(gameTimer);
      setGameState({
        action: actionsTypes.GAME_RESULT,
        payload: {
          roomName: roomName,
          users: rooms[roomName],
          username: username,
        },
      });

      const leaderBoard = getLeaderBoard(users);

      roomFacade.clearUserProgress(rooms, roomName);
      users = rooms[roomName];

      io.to(roomName).emit("GAME_RESULTS", {
        roomName,
        users,
        leaderBoard,
      });
    }
  }, 1000);
};

const checkPlayerFinishedSetState = ({
  progressBarWidth,
  rooms,
  roomName,
  username,
}) => {
  if (progressBarWidth === MAX_PROGRESS_BAR_WIDTH) {
    setGameState({
      action: actionsTypes.PLAYER_FINISHED,
      payload: {
        roomName: roomName,
        users: rooms[roomName],
        username: username,
      },
    });
  }
};

const checkCloseToFinishSetState = ({
  charsToEnd,
  rooms,
  roomName,
  username,
}) => {
  if (charsToEnd === config.CHARS_TO_END_SHOW_MESSAGE) {
    setGameState({
      action: actionsTypes.CLOSE_TO_FINISH,
      payload: {
        roomName: roomName,
        users: rooms[roomName],
        username: username,
      },
    });
  }
};

const showBotGameStatus = (timeForGame) => {
  return (
    timeForGame > 0 &&
    timeForGame % config.SECONDS_INTERVAL_BOT_SHOW_GAME_STATUS ===
      config.SECONDS_FOR_GAME % config.SECONDS_INTERVAL_BOT_SHOW_GAME_STATUS
  );
};
