import { createElement, addClass, removeClass } from "./helper.mjs";
import { socket } from "./game.mjs";
import { texts } from "./data.mjs";
import { MAX_PROGRESS_BAR_WIDTH } from "./config.mjs";

const readyButton = document.getElementById("game-window__button");
const backToRoomsButton = document.getElementById("back-to-rooms__button");
const countdownLabel = document.getElementById("countdown");
const gameTimeLeft = document.getElementById("game-time-left");

const gameTextSelected = document.getElementById("game-text-selected");
const gameText = document.getElementById("game-text");

let cursor = 0;

export const countDownBeforeStart = ({ roomName, countdownTime }) => {
  addClass(readyButton, "display-none");
  addClass(backToRoomsButton, "display-none");
  removeClass(countdownLabel, "display-none");
  countdownLabel.textContent = countdownTime;
};

export const gameTimer = ({ roomName, timeForGame }) => {
  gameTimeLeft.textContent = `${timeForGame} seconds left`;
};

export const startGame = ({ roomName, textIndex, timeForGame }) => {
  addClass(countdownLabel, "display-none");
  gameTimeLeft.textContent = `${timeForGame} seconds left`;

  // show text
  const text = texts[textIndex];
  gameText.innerText = text;
  gameTextSelected.innerText = "";

  // add keypressed listener
  cursor = 0;
  document.addEventListener("keypress", typeKey.bind(event, text, roomName));
};

export const typeKey = (text, roomName, event) => {
  if (cursor < text.length && event.keyCode === text[cursor].charCodeAt(0)) {
    gameText.innerText = text.slice(cursor + 1);
    gameTextSelected.innerText = text.slice(0, cursor + 1);

    let progressBarWidth = Math.round(
      (gameTextSelected.innerText.length / text.length) *
        MAX_PROGRESS_BAR_WIDTH
    );
    socket.emit("RIGHT_CHAR_TYPED", { roomName, progressBarWidth, charsToEnd: gameText.innerText.length });

    cursor++;
  }
};

export const finishGame = ({ roomName, leaderBoard }) => {
  addClass(gameTimeLeft, "display-none");
  gameText.innerText = "";
  gameTextSelected.innerText = "";
  
  document.removeEventListener("keypress", typeKey);

  showLeaderBoard({ roomName, leaderBoard });
};

export const showLeaderBoard = ({ roomName, leaderBoard }) => {
  const title = "LEADER BOARD";
  const bodyElement = createElement({
    tagName: "div",
    className: "leaderboard__container"
  });
  bodyElement.innerText = leaderBoard;

  removeClass(readyButton, "display-none");
  removeClass(backToRoomsButton, "display-none");
};

