import { createElement, addClass, removeClass } from "./helper.mjs";
import {
  countDownBeforeStart,
  gameTimer,
  startGame,
  finishGame,
} from "./gamePlay.mjs";
import { createRoomCard, createPlayerCard } from "./createGameElements.mjs";

export const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

export const socket = io("http://localhost:3002/game", { query: { username } });

const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");

const createRoomButton = document.getElementById("create-room-button");
const backToRoomsButton = document.getElementById("back-to-rooms__button");
const readyButton = document.getElementById("game-window__button");

const roomCardsContainer = document.getElementById("room-cards__container");
const playersContainer = document.getElementById("players__container");
const roomNameLabel = document.getElementById("room-name__label");

const changePage = ({ showGamePage }) => {
  const showPage = showGamePage ? gamePage : roomsPage;
  const hidePage = showGamePage ? roomsPage : gamePage;
  addClass(hidePage, "display-none");
  removeClass(showPage, "display-none");
};

const onClickCreateRoomButton = () => {
  const roomName = prompt("Please, enter the room name:");
  if (!roomName) {
    return;
  }

  socket.emit("CREATE_ROOM", roomName);
};

createRoomButton.addEventListener("click", onClickCreateRoomButton);

export const onClickJoinRoomButton = (roomName) => {
  socket.emit("JOIN_ROOM", roomName);
};

const onClickBackToRoomsButton = (roomName) => {
  socket.emit("BACK_TO_ROOMS", roomName);
  changePage({ showGamePage: false });
};

const onClickReadyButton = (roomName) => {
  socket.emit("READY_PRESSED", roomName);
};

const appendRoomCard = ({ roomName, joinedPlayers }) => {
  const roomCard = createRoomCard({ roomName, joinedPlayers });
  roomCardsContainer.appendChild(roomCard);
};

const initRooms = (rooms) => {
  const allRoomCards = Object.keys(rooms).map((roomName) => ({
    roomName: roomName,
    joinedPlayers: rooms[roomName].length,
  })).map(createRoomCard);
  roomCardsContainer.innerHTML = "";
  roomCardsContainer.append(...allRoomCards);
};

const initJoinedRoom = ({ roomName, users }) => {
  changePage({ showGamePage: true });

  backToRoomsButton.addEventListener("click", function () {
    onClickBackToRoomsButton(roomName);
  });
  readyButton.addEventListener("click", function () {
    onClickReadyButton(roomName);
  });

  roomNameLabel.innerText = roomName;
  updateRoomPlayers({ roomName, users });
};

const updateRoomPlayers = ({ roomName, users }) => {
  const allPlayerCards = users.map(createPlayerCard);
  playersContainer.innerHTML = "";
  playersContainer.append(...allPlayerCards);
};

const getGameResults = ({ roomName, users, leaderBoard }) => {
  updateRoomPlayers({ roomName, users });
  finishGame({ roomName, leaderBoard });
};

const roomNameExists = (roomName) => {
  alert(`Room with name ${roomName} already exists.`);
};

const clearSessionStorage = () => {
  sessionStorage.clear();
  window.location.replace("/login");
  alert("Username exists.");
};

const roomIsFull = (maxUsersInRoom) => {
  alert(`Room is full. Max number of users: ${maxUsersInRoom}`);
};

const updateBotText = ({ text }) => {
  const botCommentWindow = document.getElementById("bot-comment-window");
  botCommentWindow.innerText = text;
};

socket.on("INIT_ROOMS", initRooms);
socket.on("APPEND_ROOM_CARD", appendRoomCard);
socket.on("JOIN_ROOM_DONE", initJoinedRoom);
socket.on("UPDATE_ROOM_PLAYERS", updateRoomPlayers);
socket.on("READY_PRESSED_DONE", updateRoomPlayers);
socket.on("PLAYER_JEFT", updateRoomPlayers);

socket.on("COUNTDOWN_BEFORE_START", countDownBeforeStart);
socket.on("START_GAME", startGame);
socket.on("GAME_TIMER", gameTimer);
socket.on("UPDATE_PROGRESS_BAR", updateRoomPlayers);
socket.on("GAME_RESULTS", getGameResults);

socket.on("ROOM_NAME_EXISTS", roomNameExists);
socket.on("USERNAME_EXISTS", clearSessionStorage);
socket.on("ROOM_IS_FULL", roomIsFull);

socket.on("UPDATE_BOT_TEXT", updateBotText);
