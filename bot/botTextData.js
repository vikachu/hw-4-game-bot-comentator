class sayHello {
  constructor() {
    this.message =
      "Hello! I am a Comentator Bot. May the keyboard be with you.";
  }
}

class introducePlayers {
  constructor(usernames) {
    this.message = `Race started and today we have in game: ${usernames}.`;
  }
}

class gameStatus {
  constructor(currentLeaders) {
    this.message = `Cars are moving in order: ${currentLeaders}`;
  }
}

class closeToFinish {
  constructor(user) {
    this.message = `${user} is close to finish!`;
  }
}

class playerFinished {
  constructor(user) {
    this.message = `And ${user} finished the race. Nice try.`;
  }
}

class gameResults {
  constructor(leaders) {
    this.message = `FINISH ! Leader are: ${leaders} - in order came.`;
  }
}

const botTextType = {
  sayHello,
  introducePlayers,
  gameStatus,
  closeToFinish,
  playerFinished,
  gameResults,
};

// Factory pattern
export const getTextFactory = (type, attributes) => {
  const getText = botTextType[type];
  const text = new getText(attributes);
  return text;
};

// curring
export const getTextFactoryCurried = (type) => {
  return (attributes) => {
    const getText = botTextType[type];
    const text = new getText(attributes);
    return text;
  };
};
